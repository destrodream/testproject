import json
import redis
from flask import Flask
from datetime import datetime

app = Flask(__name__)
app.debug = True
r = redis.StrictRedis(host='136.243.95.34', port=6381, db=0)


@app.route('/endpoint')
def endpoint():
    r.set('time', str(datetime.now()))
    current_time = r.get('time')
    return json.dumps({'server_time': current_time.decode('utf-8')})

if __name__ == '__main__':
    app.run()